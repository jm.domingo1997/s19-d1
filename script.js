console.log('*** JS ES6 Updates ***');
/*
1. Exponent Operator
*/

//before
const firstNum = 8 ** 2;
console.log(firstNum);

//update
const secondNum = Math.pow(8,2);
console.log(secondNum);

/*
2. Template Literals (``)
-Allows to write strings without using the concatenation operator (+)
- Greatly help with code readability
*/

let name = "John";

//before
let message = 'Hello' + name + '! Welcome to programming!';
console.log("message without template literals: " + message);

//using template literals
message = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message}`);

//mutli-line
const anotherMessage = `${name} attended a Math competition. 
He won it by solving the problem 8 ** 2 with the
	solution of ${firstNum}`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is ${principal*interestRate}`);

/*
3. Array Destructuring
-allows to unpack elements in array into distinct variables
-allows us to name array elemts with variables instead of using index numbers
*/

const fullName = ['Juan','Dela','Cruz']

//before
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//deconstructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

/*
4. Object Destructuring
-allows to unpack properties of an object into distinct variables
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz",
	walk:  function(){
		return `${givenName} walked 1km`
	}
};

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

//using destructuring
const { givenName, maidenName, familyName, walk} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`)

console.log(walk);
console.log(walk());

// let population = {
// 	people: [
// 		p1,
// 		p2
// 	]
// };

// const {people} = population;
// const [president, secretary] = people;

/*
5. Arrow Function
-compact alternative syntax to traditional functions
-useful for code snippets in any other portion of the code
-syntax:
const variable = () => {};
*/

//before
const hello = function(){
	console.log("Hello World!");
}

//using arrow function
const helloAgain = () => {
	console.log('Hello world!')
}

hello();
helloAgain();

// Arrow functions with loops
const students =['John','Jane','Judy'];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`);
});

//using arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})


/*
6. Implicit Return Statement
- there are some instances when you can omit the "return statement"
- JS implicitly adds it for the result of the function
*/

//const add = (x,y) => { return x + y;};
const add = (x,y) => x + y;
let total = add(1,2);
console.log(total);

/*
7. Default Function Argument Value
- provides a default argument value if none is provided
*/

const greet = (name = 'User') => {
	return `Good morning, ${name}!`;
}

console.log(greet());
console.log(greet('John'));

/*
8. Class-Based Object Blueprint
- allows creation/instantiation of objects using classes as blueprints

Creating a class
- the constructor is a special method of a class for creating an object for that class
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
}

const myCar = new Car();


myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2020;

console.log(myCar)


//creating new instance of car with initialized values
const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar)










